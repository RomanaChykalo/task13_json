package com.chykalo;

import com.chykalo.comparator.FlowerComparator;
import com.chykalo.model.entity.*;
import com.chykalo.parser.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

import static com.chykalo.model.consts.PathConst.JSONPATH;

public class App {
    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        File json = new File(JSONPATH);
        printList(new JsonParser().getList(json));
    }

    private static void printList(List<Flower> flowers) {
        logger.info("JSON list:");
        flowers.stream().sorted(new FlowerComparator()).forEach(System.out::println);
    }
}

