package com.chykalo.gson;

import com.chykalo.model.entity.Flower;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Converter {
    private static Logger logger = LogManager.getLogger(Converter.class);
    GsonBuilder builder;
    Gson gson;

    public Converter() {
        this.builder = new GsonBuilder();
        this.gson = builder.create();
    }
    public void convertObjectToJsonUsingGson(Object object){
        logger.info(gson.toJson(object));
    }
    public void convertFromJsonToObjectUsingGson(String jsonText){
        Flower flower = gson.fromJson(jsonText, Flower.class);
        logger.info("GSON", "Name: " + flower.getName() + "country: " + flower.getOriginCountry() +
                "\ntype of soil: " + flower.getTypeOfSoil());
    }
}
