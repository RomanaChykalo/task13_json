package com.chykalo.parser;

import com.chykalo.model.entity.Flower;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JsonParser {
    private ObjectMapper objectMapper;

    public JsonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Flower> getList(File jsonFile){
        Flower[] flowers = new Flower[0];
        try{
            flowers = objectMapper.readValue(jsonFile, Flower[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(flowers);
    }
}
