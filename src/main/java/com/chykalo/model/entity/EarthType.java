package com.chykalo.model.entity;

public enum EarthType {
    CLAY, SOIL, TURF
}
