package com.chykalo.model.entity;

public enum Multiplying {
    LEAVES, CUTTINGS, SEEDS
}
