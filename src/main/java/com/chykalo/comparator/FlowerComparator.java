package com.chykalo.comparator;

import com.chykalo.model.entity.Flower;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {

    public int compare(Flower flower1, Flower flower2) {
        if (flower1.getTip().getTemperature() > flower2.getTip().getTemperature()) {
            return 1;
        } else if (flower1.getTip().getTemperature() < flower2.getTip().getTemperature()) {
            return -1;
        } else {
            return 0;
        }
    }

}
